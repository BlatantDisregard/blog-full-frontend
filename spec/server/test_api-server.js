const _ = require('lodash');

delete require.cache[require.resolve('../../src/helpers/api')];
const api = require('../../src/helpers/api');

describe('api on server', () => {
  describe('baseUrl', () => {
    it('is set to http://api:3000', () => {
      expect(api.baseUrl).toBe('http://api:3000');
    });
  });
});
