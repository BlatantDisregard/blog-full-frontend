const React = require('react');
const ReactDOM = require('react-dom');
const Simulate = require('simulate');

const PostView = require('../../src/components/PostView');

describe('PostView on the client', () => {
  let props;

  beforeEach(function() {
    const fixture = '<div id="fixture"></div>';

    document.body.insertAdjacentHTML(
      'afterbegin',
      fixture);

    props = {
      post: {title: 'Title', content: '*Content*'},
      time: {now: '2010-01-01T00:00:00.000Z'},
      onEdit: jasmine.createSpy('onEdit'),
      onDelete: jasmine.createSpy('onDelete')
    };

    ReactDOM.render(
      <PostView {...props}/>,
      document.getElementById('fixture')
    );
  });

  afterEach(function() {
    document.body.removeChild(document.getElementById('fixture'));
  });

  it('displays the correct title', () => {
    const titleElement = document.querySelector('.blog-post-title');
    expect(titleElement.innerHTML).toBe('Title');
  });

  it('triggers onEdit when edit button is clicked', () => {
    const editButton = document.querySelector('a > .fa-edit').parentNode;
    Simulate.click(editButton);
    expect(props.onEdit).toHaveBeenCalled();
  });

  it('triggers onDelete when delete button is clicked', () => {
    const deleteButton = document.querySelector('a > .fa-remove').parentNode;
    Simulate.click(deleteButton);
    expect(props.onDelete).toHaveBeenCalled();
  });
});
