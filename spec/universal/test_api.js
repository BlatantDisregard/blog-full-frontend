const api = require('../../src/helpers/api');
const ajax = require('../../src/helpers/ajax');

const actualBaseUrl = api.baseUrl;

describe('api', () => {
  beforeEach(() => {
    spyOn(ajax, 'request');
    api.baseUrl = '<baseUrl>';
    api.get('/posts');
  });

  afterEach(() => {
    api.baseUrl = actualBaseUrl;
  });

  describe('get', () => {
    beforeEach(() => {
      api.get('/posts');
    });

    it('generates a GET ajax request', () => {
      expect(ajax.request).toHaveBeenCalledWith({
        method: 'GET',
        url: '<baseUrl>/posts',
        json: true
      });
    });
  });
});
