const React = require('react');
const ReactDOM = require('react-dom');

const createStore = require('./helpers/createStore');
const Root = React.createFactory(require('./components/Root'));

// TODO: Uncomment lines below after completing the time reducer
const moment = require('moment');
const timeActionCreators = require('./reducers/time');

// Initialisation function called on page load
window.main = (initialState) => {
  // Create root React component with Redux store
  const store = createStore(initialState);
  const rootComponent = Root({ store });

  // Mount React root component in DOM
  const mountPoint = document.getElementById('root');
  ReactDOM.render(rootComponent, mountPoint);

  // TODO: Uncomment lines below after completing the time reducer
  window.setInterval(() => {
    const now = moment().format();
    store.dispatch(timeActionCreators.setCurrentTime(now));
  }, 10000);
};
