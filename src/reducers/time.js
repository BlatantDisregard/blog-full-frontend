const _ = require('lodash');

// TODO: Add UPDATE action type constant
const UPDATE = 'blog-frontend/time/UPDATE';

function reducer(state, action) {
  state = state || {};
  action = action || { type: null };

  switch(action.type) {
    // TODO: Add case for when the action type is UPDATE
    case UPDATE: {
      return _.assign({}, state, { now: action.timeNow });
    } break;
    default:
      return state;
  }
}

reducer.setCurrentTime = function(timeNow) {
  // TODO: Return action with type UPDATE here
  return { type: UPDATE, timeNow }
};

module.exports = reducer;
