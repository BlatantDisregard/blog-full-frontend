const path = require('path');
const express = require('express');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const _ = require('lodash');
const moment = require('moment');

const api = require('./helpers/api');
const createStore = require('./helpers/createStore');
const Root = React.createFactory(require('./components/Root'));
const combinedReducers = require('./reducers');

// Create a new Express app
const app = express();

// Use EJS for our views and tell it where to find our view files
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

// Serve up our static assets from 'dist'
app.use('/assets', express.static(path.join(__dirname,
        '..', 'dist')));

// Serve up font-awesome fonts from vendor folder
app.use('/assets/fonts', express.static(path.join(__dirname,
        '..', 'vendor', 'font-awesome', 'fonts')));

// Set up the root route
app.get('/', (req, res) => {
  api.get('/posts').then((posts) => {
    // Set initial state
    const initialState = combinedReducers();
    const postIds = _.map(posts, post => post.id);
    initialState.posts.visiblePosts = postIds;
    initialState.posts.postData = _.keyBy(posts, 'id');
    initialState.time.now = moment().format();

    // Create root React component with Redux store
    const store = createStore(initialState);
    const rootComponent = Root({ store });

    try {
      // Render root component on the server
      const reactHtml = ReactDOMServer.renderToString(rootComponent);

      // Hostname
      const hostname = require('os').hostname();

      // Convert the initial application state into a JavaScript string
      const initialStateString =
        JSON.stringify(store.getState()).replace(/<\//g, "<\\/");

      const htmlContent = `
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <title>Wow! A title!</title>
            <link rel="stylesheet" type="text/css" href="/assets/css/app.css">
            <script src="/assets/js/vendor.js"></script>
            <script src="/assets/js/app.js"></script>
          </head>
          <body>
            <div class="container" id="root">${reactHtml}</div>
            <footer class="footer" style="margin-top: 64px;">
              <div class="container" style="text-align: center;">
                <span class="text-muted">Page rendered at ${Date()} by</span>
                ${hostname}
              </div>
            </footer>
            <script>main(${initialStateString});</script>
          </body>
        </html>`;

      // Respond with the complete HTML page
      res.send(htmlContent);
    } catch(err) {
      console.error(err.stack);
      res.status(500).send(err.stack);
    }
  }).catch((err) => {
    console.error(err.stack);
    res.status(500).send('Request to API failed.');
  });
});

module.exports = app;
